import java.util.Scanner;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class CaptionCrawler {
	public static ArrayList<String> url_list = new ArrayList<String>();
	public static String output_dir = "";
    public static void getVideoCaption(String url, String outfile) throws IOException{ 
    	YoutubeCaption yc = new YoutubeCaption();
        //String subtitle = yc.getSubtitle(video_id);
        String subtitle = yc.getSubtitle(url);
        if (subtitle == null) {
        	System.out.println("File has no subtitle");
        	return;
        }
		subtitle = subtitle.replaceAll("&quot;", "\"");
		subtitle = subtitle.replaceAll("&amp;", "&");
		subtitle = subtitle.replaceAll("&#39;", "'");
		subtitle = subtitle.replaceAll("&lt;", "<");
		subtitle = subtitle.replaceAll("&gt;", ">");
		subtitle = subtitle.replaceAll("\r", " ");
		subtitle = subtitle.replaceAll("\n", " ");
		subtitle = subtitle.replaceAll("\t", " ");
             
        //Write caption to output file   
        String path = output_dir + File.separator + outfile + ".txt";                                                 
        File f = new File(path);
		f.getParentFile().mkdirs(); 
		f.createNewFile();
		FileWriter fw = new FileWriter(f.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(subtitle);
		bw.close();
		
		System.out.println("Subtitle for " + outfile + " has been successfully retrieved.");

    }
    
    public static void getVideoIds(String id_file) {
    	Scanner readIds = null;
    	try {
            readIds = new Scanner(new File(id_file));
        } catch (Exception e) {
            System.out.println("Could not locate the data file!");
        }
        int i = 0;        
        while(readIds.hasNext()) {
        	url_list.add(i++, readIds.nextLine());
        }
    }
    
    public static void getCaptionUrls(String url_file) {
    	Scanner readUrls = null;
    	try {
            readUrls = new Scanner(new File(url_file));
        } catch (Exception e) {
            System.out.println("Could not locate the data file!");
        }
        int i = 0;        
        while(readUrls.hasNext()) {  
        	String tmp = readUrls.nextLine();
        	if (!tmp.isEmpty()) 
        		url_list.add(i++, tmp);
        }
    } 
    
    public static void main(String[] args) {    	
    	CaptionCrawler.getCaptionUrls(args[0]);    	
    	output_dir = args[1];
    	for (int i = 0; i < url_list.size(); i++) {
    		String cap_url = url_list.get(i);
    		try {
    			CaptionCrawler.getVideoCaption(cap_url, Integer.toString(i));
    		} catch (IOException e) {
    			e.printStackTrace();
    		}	
    	}
    	
    }
}
