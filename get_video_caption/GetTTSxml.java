
import java.io.IOException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.io.InputStream;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class GetTTSxml {

    public NodeList getXml(String url) {
        URL thisurl = null;
        NodeList thisNodeList = null;
        try {
            thisurl = new URL(url);

            if (thisurl.openStream().read() != -1) {
                Document doc = parseXML(thisurl.openStream());
                NodeList descNodes = doc.getElementsByTagName("text");
                thisNodeList = descNodes;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return thisNodeList;
    }

    private Document parseXML(InputStream stream) {
        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();
            doc = objDocumentBuilder.parse(stream);
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        }

        return doc;
    }
}
