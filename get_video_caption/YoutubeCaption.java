

import org.w3c.dom.NodeList;

public class YoutubeCaption {

    public YoutubeCaption() {
    }

    public String getSubtitle(String url) {
        String subtitle = "";
        String strTTSurl = url;				
        GetTTSxml ttsXml = new GetTTSxml();
        NodeList nodeList = null;

        nodeList = ttsXml.getXml(strTTSurl);

        if(nodeList == null){
        	System.out.println("Something wrong with the url");
        	return null;
        }
        for (int i = 0; i < nodeList.getLength(); i++) {
            subtitle += nodeList.item(i).getTextContent() + " ";
        }

        return subtitle;
    }
}
